"""Sphinx extension for the hereon-netcdf binding rules."""
import os
import os.path as osp
from io import StringIO
from shutil import copyfile
from pathlib import Path
import textwrap

import yaml
import glob

from docutils import nodes
from docutils.parsers.rst import Directive, directives
from docutils.statemachine import StringList
from sphinx.util.docutils import SphinxDirective, new_document
from sphinx.locale import _
from sphinx.errors import NoUri
from sphinx.addnodes import pending_xref

from cffconvert.cli.create_citation import create_citation
from cffconvert.cli.validate_or_write_output import validate_or_write_output


__version__ = "0.0.1a1"

__author__ = "Philipp S. Sommer"
__copyright__ = "Copyright (C) 2021 Helmholtz-Zentrum Hereon"

__credits__ = ["Philipp S. Sommer"]
__license__ = "Apache-2.0"

__maintainer__ = "Philipp S. Sommer"
__email__ = "hcdc_support@hereon.de"

__status__ = "Pre-Alpha"


class md_entry(nodes.Admonition, nodes.Element):
    pass


class md_entries(nodes.General, nodes.Element):
    pass


def visit_md_entry_node(self, node):
    self.visit_admonition(node)


def depart_md_entry_node(self, node):
    self.depart_admonition(node)


def purge_md_entries(app, env, docname):
    """Update updated metadata entries."""
    if not hasattr(env, "md_entries"):
        return

    env.md_entries = [
        entry
        for entry in env.md_entries
        if entry.get("docname", "") != docname
    ]


def merge_md_entries(app, env, docnames, other):
    """Merge metadata entries for parallel builds."""
    if not hasattr(env, "md_entries"):
        env.md_entries = []

    if hasattr(other, "md_entries"):
        env.md_entries.extend(other.md_entries)


def resolve_reference(self, md_entry_node: md_entry, docname: str) -> None:
    """Resolve references in the todo content."""
    for node in md_entry_node.traverse(addnodes.pending_xref):
        if "refdoc" in node:
            node["refdoc"] = docname

    # Note: To resolve references, it is needed to wrap it with document node
    self.document += md_entry_node
    self.env.resolve_references(self.document, docname, self.builder)
    self.document.remove(md_entry_node)


def process_md_entries(app, doctree, docname):
    # Replace all md_entries nodes with a list of collected entries
    env = app.builder.env

    if not hasattr(env, "md_entries"):
        env.md_entries = []

    dummy_document = new_document("")

    for node in doctree.traverse(md_entries):

        titles = [
            _("Attribute"),
            _("Description/Format"),
            _("Specification/Example"),
        ]

        table, tbody = prepare_metadata_table(titles)

        for md_entry_info in env.md_entries:
            # Insert into the todolist
            row = md_entry_info["md_entry"].deepcopy()

            # resolve pending references to allow :ref: statements in the
            # description etc.
            # Mimics the sphinx.ext.todo.TodoListProcessor.resolve_reference
            for xref in row.traverse(pending_xref):
                if "refdoc" in xref:
                    xref["refdoc"] = docname
            dummy_document += row
            env.resolve_references(dummy_document, docname, app.builder)
            dummy_document.remove(row)

            tbody.append(row)

        node.replace_self([table])


def prepare_metadata_table(titles):
    tgroup = nodes.tgroup(cols=3)
    for width in [30, 30, 30]:
        tgroup += nodes.colspec(colwidth=width)

    header = nodes.row()
    for title in titles:
        header += nodes.entry("", nodes.paragraph(text=title))

    tgroup += nodes.thead("", header)

    tbody = nodes.tbody()
    tgroup += tbody
    return nodes.table("", tgroup, classes=["nc-attr-table"]), tbody


class MetaDataTableDirective(SphinxDirective):
    """Transform a YAML file into a metadata table."""

    required_arguments = 1

    def run(self):
        srcdir = self.state.document.settings.env.srcdir

        data_file = self.arguments[-1]

        with open(osp.join(srcdir, data_file)) as f:
            data = yaml.load(f, Loader=yaml.SafeLoader)

        if not hasattr(self.env, "md_entries"):
            self.env.md_entries = []

        titles = [
            _("Attribute"),
            _("Description/Format"),
            _("Specification/Example"),
        ]

        table, tbody = prepare_metadata_table(titles)

        docname = self.env.docname

        for key, attrs in data.items():
            targetid = f"md-{key}"
            targetnode = nodes.target("", "", ids=[targetid], names=[targetid])
            self.state.document.note_explicit_target(targetnode)

            row = nodes.row()

            # Create a reference
            para = nodes.paragraph()
            newnode = nodes.reference("", "")
            innernode = nodes.emphasis(key, key)

            newnode["refdocname"] = docname

            try:
                newnode["refuri"] = self.env.app.builder.get_target_uri(
                    docname
                )
            except NoUri:
                pass
            else:
                newnode["refuri"] += "#" + targetid
            newnode.append(innernode)
            para += newnode

            row += nodes.entry("", para, classes=["nc-attr-name"])

            description = nodes.paragraph()
            try:
                self.state.nested_parse(
                    StringList(attrs["description"].splitlines()), 0, description
                )
            except:
                pass

            row += nodes.entry(
                "", description, classes=["nc-attr-description"]
            )

            example = attrs.get("example", "")
            if isinstance(example, str):
                paragraph = nodes.paragraph()
                self.state.nested_parse(StringList([example]), 0, paragraph)
                row += nodes.entry("", paragraph, classes=["nc-attr-example"])
            else:
                paragraph = nodes.paragraph()
                examples = []
                if example:
                    examples = ["- " + example for example in example]
                self.state.nested_parse(StringList(examples), 0, paragraph)
                try:
                    row += nodes.entry(
                        "", paragraph.children[0], classes=["nc-attr-example"]
                    )
                except:
                    pass

            self.env.md_entries.append(
                {
                    "lineno": self.lineno,
                    "md_entry": row.deepcopy(),
                    "target": targetnode,
                }
            )
            tbody += targetnode
            tbody += row

        return [table]


IMAGE_BASE = """
.. image:: /_static/orcid.*
    :target: %s
    :width: 16px
"""


class AuthorList(SphinxDirective):
    """A list of authors."""

    has_content = True

    option_spec = {"all": directives.flag, "affiliations": directives.flag}

    def run(self):
        """Display a list of authors"""
        para: nodes.Element = nodes.paragraph(translatable=False)
        emph = nodes.emphasis()
        para += emph

        all_messages = []
        citation_file = self.env.app.config["citation_file"]

        with open(osp.join(self.env.app.srcdir, citation_file)) as f:
            citation_info = yaml.safe_load(f)

        all_authors = citation_info["authors"]
        all_author_names = [
            "%(given-names)s %(family-names)s" % d for d in all_authors
        ]
        authors = list(map(str, self.content))

        if "all" in self.options:
            authors.extend(a for a in all_author_names if a not in authors)

        affiliations = {}
        for author in authors:
            if author in all_author_names:
                affiliation = all_authors[all_author_names.index(author)].get(
                    "affiliation"
                )
                if affiliation:
                    affiliations[affiliation] = f"affil{len(affiliations)}"
        display_affiliations = affiliations and "affiliations" in self.options

        for i, author in enumerate(authors, 1):
            inodes, messages = self.state.inline_text(author, self.lineno)
            emph += inodes
            all_messages += messages
            if author in all_author_names:
                author_props = all_authors[all_author_names.index(author)]
                if author_props and "orcid" in author_props:
                    image_directive = IMAGE_BASE % author_props["orcid"]
                    paragraph = nodes.paragraph()
                    self.state.nested_parse(
                        StringList(image_directive.splitlines()), 0, paragraph
                    )
                    emph += self.state.inline_text(" ", self.lineno)[0]
                    emph += paragraph.children[0]
                    all_messages += messages
                if display_affiliations and author_props.get("affiliation"):
                    affil_key = affiliations[author_props.get("affiliation")]
                    emph += self.state.inline_text(
                        r"\ [#%s]_ \ " % affil_key, self.lineno
                    )[0]
            if i < len(authors):
                emph += self.state.inline_text(", ", self.lineno)[0]

        ret = [para]

        if display_affiliations:
            footnotes = nodes.paragraph()
            lines = [
                f".. [#{key}] {affil}" for affil, key in affiliations.items()
            ]
            self.state.nested_parse(StringList(lines), 0, footnotes)
            ret += footnotes

        return ret + all_messages


CFF_FORMATS = [
    "bibtex",
    "codemeta",
    "endnote",
    "ris",
    "cff",
    "schema.org",
    "zenodo",
    "apalike",
]


def validate_cff_format(argument) -> str:
    """Validator function for CFF_FORMATS."""
    return directives.choice(argument, CFF_FORMATS)


class CitationInfo(SphinxDirective):
    """A directive to display the citation info."""

    option_spec = {"format": validate_cff_format}

    def convert_citation(self, citation, outputformat) -> str:
        """Convert the citation to a string."""
        formatters = {
            "apalike": citation.as_apalike,
            "bibtex": citation.as_bibtex,
            "cff": citation.as_cff,
            "codemeta": citation.as_codemeta,
            "endnote": citation.as_endnote,
            "ris": citation.as_ris,
            "schema.org": citation.as_schemaorg,
            "zenodo": citation.as_zenodo,
        }
        return formatters[outputformat]()

    def get_lexer(self, outputformat):
        lexers = {
            "apalike": "none",  # is ignored anyway
            "bibtex": "bibtex",
            "cff": "yaml",
            "codemeta": "json",
            "endnote": "none",
            "ris": "none",
            "schema.org": "json",
            "zenodo": "json",
        }
        return lexers[outputformat]

    def run(self):
        """Display the converted text format."""

        citation_file = self.env.app.config["citation_file"]
        outputformat = self.options.get("format", "apalike")

        citation = create_citation(
            osp.join(self.env.app.srcdir, citation_file), None
        )
        formatted = self.convert_citation(citation, outputformat)
        if formatted == "apalike":
            return [nodes.Text(formatted.strip())]
        else:
            para: nodes.Element = nodes.paragraph(translatable=False)
            lexer = self.get_lexer(outputformat)
            code = [".. code-block:: %s" % lexer, ""] + textwrap.indent(
                formatted, "    "
            ).splitlines()
            self.state.nested_parse(StringList(code), 0, para)
            return [para]


class MetaDatalistDirective(Directive):
    """Display a list of all metadata entries."""

    def run(self):
        return [md_entries("")]


def copy_custom_files(app):
    outdir = Path(osp.join(app.builder.outdir, "_static"))
    srcdir = Path(osp.join(osp.dirname(__file__), "_static"))
    os.makedirs(outdir, exist_ok=True)
    if app.builder.format == "html":
        fname = "nc-table-styles.css"
        outfile = outdir / fname
        infile = srcdir / fname
        copyfile(str(infile), str(outfile))
    static_src_path = app.config.html_static_path[0]
    outdir = Path(app.srcdir) / static_src_path
    for fname in glob.glob(str(srcdir / "orcid.*")):
        copyfile(fname, outdir / osp.basename(fname))


def visit_pending_xref(self, node):
    # type: (nodes.Node) -> None
    pass


def depart_pending_xref(self, node):
    # type: (nodes.Node) -> None
    pass


def setup(app):
    app.add_node(md_entries)
    app.add_node(
        md_entry,
        html=(visit_md_entry_node, depart_md_entry_node),
        latex=(visit_md_entry_node, depart_md_entry_node),
        text=(visit_md_entry_node, depart_md_entry_node),
    )

    app.add_config_value("citation_file", "../CITATION.cff", "html")
    app.config.html_static_path.append(
        osp.join(osp.dirname("__file__"), "_static")
    )

    app.connect("doctree-resolved", process_md_entries)
    app.connect("env-purge-doc", purge_md_entries)
    app.connect("env-merge-info", merge_md_entries)

    app.add_directive("authorlist", AuthorList)
    app.add_directive("citation-info", CitationInfo)
    app.add_directive("metadata_table", MetaDataTableDirective)
    app.add_directive("metadata_list", MetaDatalistDirective)
    app.connect("builder-inited", copy_custom_files)
    app.add_css_file("nc-table-styles.css")

    return {
        "version": "0.1",
        "parallel_read_safe": True,
        "parallel_write_safe": True,
    }
